"""
Copyright (c) 2021 Sonus Dept. / Valerio Orlandini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from ctypes import *
from pylibpd import *
import argparse
import array
import librosa
import json
import mido
import py_midicsv
import soundfile
import sys

class Midi:
    def __init__(self, midi_file):
        self.file = midi_file
        self.csvlist = py_midicsv.midi_to_csv(midi_file)
        self.midomidi = mido.MidiFile(midi_file)
        self.events = []
        self.ppq = self.midomidi.ticks_per_beat
        self.length = self.midomidi.length
        self.tempo = 0
        for l in range(len(self.csvlist)):
            self.events.append(self.csvlist[l].split(', '))
            if self.events[l][2] == 'Tempo' and self.tempo == 0:
                self.tempo = int(self.events[l][3])
        if self.tempo == 0:
            self.tempo = 500000
        self.tick_length = mido.tick2second(1, self.ppq, self.tempo)


def pd2wav(patch_in, midi_in = '', audio_in = '', wave_out = '', preset = '',
           length = 'auto', sample_rate = 44100, in_channels = 2,
           out_channels = 2):
    if sample_rate <= 0:
        print('Invalid sample rate, set to 44100 Hz')
        sample_rate = 44100

    # Start the engine
    m = PdManager(in_channels, out_channels, sample_rate, 1)
    block_size = libpd_blocksize()
    
    # Load the patch
    if '/' in patch_in:
        patch_file = patch_in.split('/')[-1]
        patch_path = '/'.join(patch_in.split('/')[:-1])
    else:
        patch_file = patch_in
        patch_path = '.'
    patch = libpd_open_patch(patch_file, patch_path)

    # Open preset file and send the required values to the patch
    if not preset == '':
        try:
            preset_file = preset.split(':')[0]
            json_data = json.load(open(preset_file))
            if len(preset.split(':')) > 1:
                preset_name = preset.split(':')[1]
                for i in json_data[preset_name]:
                    value = json_data[preset_name][i]
                    if isinstance(value, (int, float)):
                        libpd_float(i, value)
                    else:
                        libpd_message(i, value)
            else:
                for i in json_data:
                    value = json_data[i]
                    if isinstance(value, (int, float)):
                        libpd_float(i, value)
                    else:
                        libpd_message(i, value)
        except:
            print('Parameters file not found or not a valid JSON file')

    block_length = block_size / sample_rate

    output = []

    # Create input and output arrays
    outbuf = array.array('f', [0.0] * block_size * out_channels)
    inbuf = array.array('f', [0.0] * block_size * in_channels)

    elapsed_time = 0.0

    process_midi = False
    midi_length = 0
    process_audio = False
    audio_length = 0

    # Open MIDI file and sort events by time
    if not midi_in == '':
        try:
            midi_in = Midi(midi_in)
            tick_length = midi_in.tick_length
            midi_events = midi_in.events
            for e in midi_events:
                e[1] = int(e[1]) * tick_length
            midi_events.sort(key=lambda x: x[1])
            midi_length = midi_in.length
            process_midi = True
        except:
            print('Unable to read MIDI file')

    # Open audio and put its samples inside blocks
    if not audio_in == '':
        try:
            audio_data, sr = librosa.load(audio_in, sr = sample_rate,
                                          mono = False)
            audio_samples = len(audio_data[0])
            if audio_samples > 0:
                process_audio = True
                audio_ch = len(audio_data)
                audio_length = audio_samples / sample_rate
                sample = 0
                audio_blocks = []
                while sample < audio_samples:
                    current_block = []
                    for s in range(block_size):
                        if sample + s < audio_samples:
                            for c in range(in_channels):
                                if c < audio_ch:
                                    current_block.append(
                                        audio_data[c][sample + s])
                                else:
                                    current_block.append(0.0)
                        else:
                            for c in range(in_channels):
                                current_block.append(0.0)
                    audio_blocks.append(current_block)
                    sample += block_size
        except:
            print('Unable to read audio')

    processing_length = max(midi_length, audio_length)

    if not length == 'auto':
        if length == 'midi':
            processing_length = midi_length
        elif length == 'audio':
            processing_length = audio_length
        else:
            try:
                processing_length = float(length)
            except:
                print('Invalid length request')

    block = 0

    # Run the patch
    while elapsed_time <= processing_length:
        inbuf = array.array('f', )
        if process_midi:
            for e in midi_events:
                if elapsed_time >= e[1]:
                    if e[2] == 'Note_on_c':
                        libpd_noteon(int(e[3]), int(e[4]), int(e[5]))
                    if e[2] == 'Note_off_c':
                        libpd_noteon(int(e[3]), int(e[4]), 0)
                    if e[2] == 'Control_c':
                        libpd_controlchange(int(e[3]), int(e[4]), int(e[5]))
                    if e[2] == 'Program_c':
                        libpd_programchange(int(e[3]), int(e[4]))
                    midi_events.remove(e)
                else:
                    break
        if process_audio and block < len(audio_blocks):
            inbuf = array.array('f', audio_blocks[block])
            block += 1
        else:
            inbuf = array.array('f', [0.0] * block_size * in_channels)
        libpd_process_float(1, inbuf, outbuf)
        elapsed_time += block_length
        s = 0
        while s < len(outbuf):
            if out_channels > 1:
                outframe = []
                for c in range(out_channels):
                    outframe.append(outbuf[s + c])
                output.append(outframe)
                s += out_channels
            else:
                output.append(outbuf[s])
                s += 1

    # Write the output
    soundfile.write(wave_out, output, sample_rate, subtype = 'PCM_24')

    return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = 'Render Pure Data patch \
                                     to audio file offline')

    parser.add_argument('PATCH',
                        help = 'Pure Data patch to load')
    parser.add_argument('-m', '--midi',
                        help = 'MIDI file to be played',
                        default = '')
    parser.add_argument('-a', '--audio',
                        help = 'Audio file to be processed',
                        default = '')
    parser.add_argument('-f', '--output',
                        help = 'Output audio file, default out.wav',
                        default = 'out.wav')
    parser.add_argument('-p', '--preset',
                        help = 'JSON file with one or more \
                        presets followed by a colon and the preset name \
                        (e.g. presets.json:bass). Each preset is an object \
                        of the JSON file. If the JSON file is itself a \
                        single preset, no preset name has to be specified',
                        default = '')
    parser.add_argument('-r', '--rate',
                        help='Sample rate, default 44100',
                        type = int,
                        default = 44100)
    parser.add_argument('-in', '--channels_in',
                        help = 'Input channels, default 2',
                        type = int,
                        default = 2)
    parser.add_argument('-out', '--channels_out',
                        help = 'Output channels, default 2',
                        type = int,
                        default = 2)
    parser.add_argument('-l', '--length',
                        help = 'Length of the output. It can be expressed in \
                        seconds or it can be one among "auto" (length \
                        determined by the longest between audio and MIDI \
                        input), "audio" and "midi" (length determined by the \
                        length of the audio or the MIDI input respectively). \
                        If no audio or MIDI input files are present and no \
                        length is specified, the patch do not run at all',
                        default = 'auto')

    args = parser.parse_args()

    pd2wav(patch_in = args.PATCH, midi_in = args.midi, audio_in = args.audio,
           wave_out = args.output, preset = args.preset, length = args.length,
           sample_rate = args.rate, in_channels = args.channels_in,
           out_channels = args.channels_out)
