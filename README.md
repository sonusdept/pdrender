# pdrender

A simple Python script to render offline Pure Data (vanilla) patches, with MIDI, audio input and presets support.

## Installation

This program runs on Linux, OSX, Windows and probably also on other operating systems.

Clone this repository on your computer, with the command:
`git clone https://gitlab.com/sonusdept/pdrender.git`

Enter the directory:
`cd pdrender`

Install the dependencies:
`pip install -r requirements.txt`

Build and install libpd and its Python bindings. Go to [libpd website](https://github.com/libpd/libpd) for detailed instructions.

You are ready to use _pdrender_.

## Usage

Basically, pdrender takes a Pure Data vanilla patch and generates an audio file. The audio file contains what the patch sends to `dac~`. To make this process effectively useful, you can specify some additional stuff:
- An audio input file, which is sent to the patch `adc~`
- A MIDI input file, whose note messages, control change messages and program change messages are sent to the patch `notein`, `ctlin`, `pgmin` respectively
- A JSON file that can store one or more presets. A preset is a list of symbols with their respective values, that are sent to the patch as soon as it is opened. For example, a preset that looks like this:
`{'freq': 440.0, 'shape': 'saw'}`
will send 440.0 to the patch `receive freq` and saw to the patch `receive shape`.
- The length of the rendering. It can be a number (in seconds, floating point values are fine), _auto_ (length is the same of the input audio or MIDI file, if both are specified, the longest one is chosen), _midi_ (length is the same of the input MIDI file) or _audio_ (length is the same of the input audio file). If no length is specified, _auto_ is the default option
- The number of inputs and outputs, that to avoid weird results should be the same of the patch `adc~` outlets and `dac~` inlets respectively.
- A name for the output audio file.

MIDI and audio inputs are read synchronously, so for example you can use a MIDI file for precise automation of the patch parameters that manipulates an audio.

In practice, you can run pdrender from the command line with:

`python3 pdrender.py [-m MIDI] [-a AUDIO] [-f OUTPUT] [-p PRESET] [-r RATE] [-in CHANNELS_IN] [-out CHANNELS_OUT] [-l LENGTH] PATCH`

